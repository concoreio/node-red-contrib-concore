module.exports = function(RED) {
    function ConcoreMolecule(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        this.moleculoid = config.moleculoid;

        this.on('input', function(msg) {
            if (msg.payload == null || typeof msg.payload != 'object') {
                node.error("invalid input", msg);
                return;
            }

            if (!msg.Concore) {
                node.error("não foi encontrado o SDK no msg.Concore. Você deve adicionar o nó de Middleware neste fluxo antes deste nó.");
                return;
            }


            var Molecule = msg.Concore.Datacore.Molecule;

            var createdMolecule;
            if (msg.payload.atoms) {
                // tenta inflar molécula para atualizar.
                if (msg.payload.ACL) {
                    delete msg.payload.ACL;
                }

                createdMolecule = Molecule._inflate(this.moleculoid, msg.payload);
            } else {
                createdMolecule = new Molecule(this.moleculoid, msg.payload);
            }

            node.status({fill:"blue",shape:"dot",text:"saving..."});
            createdMolecule.save()
                .then(function(response) {
                    msg.payload = response.toJSON();
                    node.status({fill:"green",shape:"dot",text:"connected"});
                    node.send(msg);
                })
                .catch(function(error) {
                    node.status({fill:"red",shape:"dot",text:"error"});
                    node.error(error);
                });
        });
    }

    RED.nodes.registerType("molecule", ConcoreMolecule);
}