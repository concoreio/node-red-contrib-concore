module.exports = function(RED) {
  function ConcoreAmqpConfig(n) {
    RED.nodes.createNode(this,n);
    this.host = n.host;
    this.port = n.port;
    this.user = n.user;
    this.password = n.password;
  }

  RED.nodes.registerType("concore-amqp-config", ConcoreAmqpConfig);
}