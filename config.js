var crypto = require('crypto');
module.exports = function(RED) {
    function ConcoreConfig(n) {
        RED.nodes.createNode(this,n);
        this.host = n.host;
        this.appId = n.appId;
        this.apiKey = n.apiKey;
        this.masterKey = crypto.createHash('sha1')
            .update(this.appId + '' + this.apiKey, 'utf8')
            .digest('hex');
        this.user = n.user;
        this.password = n.password;
    }

    RED.nodes.registerType("concore-config", ConcoreConfig);
}