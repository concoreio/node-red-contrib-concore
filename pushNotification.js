var axios = require('axios');
var URL = require('url').URL;
module.exports = function(RED) {
  function ConcorePushNotification(config) {
    RED.nodes.createNode(this,config);
    var node = this;

    this.concoreConfig = RED.nodes.getNode(config.config);
    this.concore = this.concoreConfig.concore;

    if (!this.concore) {
      node.error('Concore could not be initialized.');
      return;
    }

    node.status({fill:"blue",shape:"dot",text:"checking connection..."});

    this.concoreConfig.checkConnection()
      .then(function(response) {
        node.concore = node.concoreConfig.concore;
        node.status({fill:"green",shape:"dot",text:"connected"});
      })
      .catch(function(error) {
        node.error(error);
        node.status({fill:"red",shape:"dot",text:"disconnected"});
      });

    const appId = this.concoreConfig.appId;
    const serverURL = new URL(this.concoreConfig.host);
    const parseURL = serverURL.protocol + '//' + serverURL.hostname + '/' + appId;
    this.request = axios.create({
      baseURL: parseURL,
      headers: {
        'X-Parse-Application-Id': appId,
        'X-Parse-Master-Key': this.concoreConfig.masterKey,
      }
    });

    this.on('input', function(msg) {
      if (msg.payload == null || typeof msg.payload != 'object') {
        node.error("invalid input", msg);
        return;
      }

      var users = msg.payload.users;
      var message = msg.payload.message;
      var title = msg.payload.title;
      var payload = msg.payload.payload || {};

      if (!Array.isArray(users)) {
        return node.error("Invalid input users. Must be an array of ids");
      }

      if (!message || typeof message !== 'string') {
        return node.error("Invalid input message. Message must be an string");
      }

      if (!title || typeof title !== 'string') {
        return node.error("Invalid input title. Title must be an string");
      }

      this.request.post('/push', {
        where: {
          user: {
            '$in': users.map(function (userId) {
              return { __type: "Pointer", className: "_User", objectId: userId };
            })
          }
        },
        data: {
          alert: message,
          title: title,
          data: {
            payload: payload
          },
        },
      })
      .then(function(response) {
        msg.payload = response;
        node.send(msg);
      })
      .catch(function(error) {
        node.error(error);
      });
    });
  }

  RED.nodes.registerType("pushNotification", ConcorePushNotification);
}
