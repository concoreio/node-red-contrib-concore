
module.exports = function(RED) {
  function Concore(config) {
    RED.nodes.createNode(this,config);
    var node = this;

    // Retrieve the config node
    this.concoreConfig = RED.nodes.getNode(config.config);
    this.concore = require('concore-sdk-js/node');

    if (!this.concore) {
       node.error('Concore could not be initialized.');
       return;
    }

    node.status({fill:"blue",shape:"dot",text:"checking connection..."});

    this.init = function(msg) {
      if (!this.concore.appId || this.concore.appId != this.concoreConfig.appId) {
        node.status({fill:"blue",shape:"dot",text:'SDK is not initialized or appId has changed'});
        this.concore.init(this.concoreConfig.host, this.concoreConfig.appId, this.concoreConfig.apiKey);

        if (this.concore.Datacore.Auth.isLogged()) {
          node.status({fill:"blue",shape:"dot",text:'LogoutSync'});
          this.concore.Datacore.Auth.logoutSync();
        }
      }

      if (this.concore.Datacore.Auth.isLogged()) {
        const loggedUser = this.concore.Datacore.Auth.getUser().getAtoms("username");
        node.status({fill:"blue",shape:"dot",text:`${loggedUser} is logged`});

        if (loggedUser === this.concoreConfig.user) {
          node.status({fill:"blue",shape:"dot",text:`${loggedUser} alredyLogged, skipping ...`});
          return Promise.resolve();
        } else {
          node.status({fill:"blue",shape:"dot",text:`${loggedUser} is not ${this.concoreConfig.user}, logoutSync`});
          this.concore.Datacore.Auth.logoutSync();
        }
      }

      if (this.concoreConfig.user && this.concoreConfig.password) {
        node.status({fill:"blue",shape:"dot",text:`loggin ${this.concoreConfig.user}...`});
        return this.concore.Datacore.Auth.login(this.concoreConfig.user, this.concoreConfig.password);
      }

      if (msg && msg.req && msg.req.headers['concore-session-token']) {
        // Faz login com o session token do usuário.
        node.status({fill:"blue",shape:"dot",text:`becoming ${msg.req.headers['concore-session-token']}`});
        return this.concore.Datacore.Auth.become(msg.req.headers['concore-session-token']);
      }

      return Promise.resolve();
    }

    this.checkConnection = function() {
      return this.init()
        .then(function() {
          return node.concore.Datacore.Moleculoid.get();
        });
    };

    this.checkConnection()
      .then(function(response) {
        node.status({fill:"green",shape:"dot",text:`${node.concore.Datacore.Auth.getUser().getAtoms('username')} logged`});
        node.concore = node.concore;
      })
      .catch(function(error) {
        node.error(error);
        node.status({fill:"red",shape:"dot",text:"disconnected"});
      });

    this.on('input', function(msg) {
      this.init(msg).then(function() {
        node.status({fill:"green",shape:"dot",text:`${node.concore.Datacore.Auth.getUser().getAtoms('username')} logged`});
        msg.Concore = node.concore;
        node.send(msg);
      });
    });

  }

  RED.nodes.registerType("concore-middleware", Concore);
};
