var amqp = require('amqplib');
var RabbitMQAdapter = require('datacore-rabbitmq-adapter');

module.exports = function(RED) {
  function ConcoreAmqp(config) {
    RED.nodes.createNode(this, config);
    const amqpConfig = RED.nodes.getNode(config.config);
    const url = 'amqp://' + amqpConfig.user + ':' + amqpConfig.password + '@' + amqpConfig.host + ':' + amqpConfig.port;
    const appId = config.appId;
    const queue = config.queue;
    const molecule = config.molecule || '#';

    console.log(url);
    this.status({ fill: 'red', shape: 'ring', text: 'checking connection...' });
    amqp.connect(url, { noDelay: true })
      .then(connect => {
        this.status({ fill: 'green', shape: 'dot', text: 'connected' });
        connect.close();
      })
      .catch(() => {
        this.status({ fill: 'red', shape: 'dot', text: 'connection invalid' });
      });

    const rabbitmq = new RabbitMQAdapter({
      url,
      options: {
        noDelay: true,
      }
    }, {
      appId: appId,
    });

    rabbitmq.on(queue, molecule, (error, message) => {
      this.send({
        payload: message
      });
    })
    .then(unbind => {
      this.on('close', () => {
        console.log('closed');
        unbind();
      });
    });
  }

  RED.nodes.registerType("concore-amqp", ConcoreAmqp);
};